<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Carbon;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table)
        {

            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password')->nullable(true);
            $table->string('phone')->nullable(true);
            $table->string('photo')->nullable(true);
            $table->integer('user_type');
            $table->date('registered_at')->default(Carbon::now());
            $table->integer('portfolio_amount')->default('0');
            $table->string('email_code')->nullable(true);
            $table->integer('registered')->default('0');
            $table->rememberToken();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
